var pipes = [];
var birds = [];
var finished = [];
var generationRate = 50;
const offset = 150;
var velocity = 10;
const pipeWidth = 100;
const gravity = 0.8;
const pushUp = 10;
const population = 500;
var generation = 1;
var c = 0;
var b = false
var bird;
var score = 0;
var sumScores = 0;
var frSlider;
var unormalized = [];
var checkbox;
var render = true;
var best = 0;

function setup(){
	createCanvas(600, 400);
	background(0);
	pipes.push(new Pipe());
	for (var i = 0; i < population; i++) {
		birds.push(new Bird());
	}
	frSlider = createSlider(1, 10, 1);
	checkbox = createCheckbox('Renderizar ', true);
 	checkbox.changed(toggleRender);

}

function draw(){
	createCanvas(600, 400);
	background(0);
	for (var j = 0; j < frSlider.value(); j++) {
		if(c > generationRate){
			pipes.push(new Pipe());
			c = 0;
		}
		
		c++;
		if(pipes.length > 0 && pipes[0].x + pipes[0].pipeWidth < 0){
			pipes.splice(0, 1);
		}
		for (var i = 0; i < pipes.length; i++) {
			pipes[i].calculate();
		}
		for (var i = 0; i < birds.length; i++) {
			birds[i].calculate();
			birds[i].collides();
		}
		if(finished.length == population){
			processGame();
		}
		score++;
	}
	if(render){
		for (var i = 0; i < birds.length; i++) {
			birds[i].show();
		}
		for (var i = 0; i < pipes.length; i++) {
			pipes[i].show();
		}
	}
	fill(255, 0, 255, 200);
	textSize(20)
	textAlign(RIGHT)
	text("Puntaje: " + score, width - 100, 20)	
	text("Mejor: " + best, width - 100, 40)	
	text("Generación: " + generation, width - 100, 60)
	

}

function toggleRender(){
	render = this.checked()
}

function processGame(){
	
	if(sumScores > 0){
		for (var i = 0; i < finished.length; i++) {
			unormalized.push(finished[i].score)
			finished[i].score /= sumScores;
		}
	}

	createNewGeneration();
}

function createNewGeneration(){
	for (var i = 0; i < population; i++) {
		var parent = selectParent();
		birds.push(parent);
	}
	finished = [];
	generation++;
	resetGame();
}

function selectParent(){
	let index = 0;

  // Pick a random number between 0 and 1
	  let r = random(1);

	  // Keep subtracting probabilities until you get less than zero
	  // Higher probabilities will be more likely to be fixed since they will
	  // subtract a larger number towards zero
	  while (r > 0) {
	    r -= finished[index].score;
	    // And move on to the next
	    index += 1;
	  }

	  // Go back one
	  index -= 1;

	  // Make sure it's a copy!
	  // (this includes mutation)
	  return finished[index].copy();
}
function resetGame(){
	pipes = [];
	score = 0;
	sumScores = 0;
	generationRate = max(20, generationRate - 0.5);
	velocity = min(20, velocity + 0.2);
}

function keyReleased(){
	b = true;
}

function Bird(brain){
	this.brain = brain;
	this.score;
	this.y = height/2 - 10;
	this.x = 100;
	this.velocity = 0;
	this.gravity = gravity;
	this.score = 0;
	
	if (brain instanceof NeuralNetwork) {
      this.brain = brain.copy();
      this.brain.mutate(mutate);
    } else {
      this.brain = new NeuralNetwork(10, 18, 2);
    }
	
	this.show = function(){
		fill(255, 255, 255, 100);
		stroke(255);
		ellipse(this.x, this.y, 20, 20);
	}

	this.calculate = function(){
		this.velocity -= this.gravity;
		this.y = min(height, this.y - this.velocity);
		this.y = max(0, this.y);
		if(this.y == height || this.y == 0)
			this.velocity = 0;
		var result = this.brain.predict(this.calculateInput())
		if(result[0] > result[1])
			this.pushUp()
	}
	this.pushUp = function(){
		this.velocity = pushUp;
	}

	this.calculateInput = function(){
		var i = 0;
		var inputs = [];
		while(!(pipes.length <= i) && pipes[i].x + pipeWidth < this.x)
			i++;
		if(pipes.length > i){
			inputs.push(pipes[i].x / width);
			inputs.push(pipes[i].top / height);
			inputs.push(pipes[i].bottom / height);
		}else{
			inputs.push(-1);
			inputs.push(-1);
			inputs.push(-1);	
		}
		if(typeof(pipes[i+1]) != 'undefined'){
			inputs.push(pipes[i+1].x / width);
			inputs.push(pipes[i+1].top / height);
			inputs.push(pipes[i+1].bottom / height);
		}else{
			inputs.push(-1);
			inputs.push(-1);
			inputs.push(-1);	
		}
		inputs.push(this.y / height);
		inputs.push(this.velocity/10 - 5);
		inputs.push(velocity);
		inputs.push(generationRate)
		return inputs;
	}

	this.collides = function(){
		_this = this
		for (var i = 0; i < pipes.length; i++) {
			if(pipes[i].collides(this)){
				this.score = pow(score, 2);
				sumScores += this.score;
				var realScore = pow(this.score, 0.5)
				if(realScore > best){
					best = realScore;
				}
				finished.push(this);
				birds = birds.filter(function(bird){
					return bird != _this;
				})
			}
		}
	}

	this.copy = function(){
		return new Bird(this.brain.copy());
	}
}

function Pipe(){
	this.x = width;
	this.pipeWidth = pipeWidth;
	this.offset = offset;
	this.top = floor(random() * (height - (offset + 100)) + 50);
	this.bottom = this.top + offset;
	this.velocity = velocity;
	this.scored = false;

	this.show = function(){
		fill(255);
		rect(this.x, 0, this.pipeWidth, this.top);
		rect(this.x, this.bottom, this.pipeWidth, height - this.bottom);
	}
	this.calculate = function(){
		this.x -= this.velocity;
	}
	this.collides = function(bird){
		var top, bottom, front;
		top = {};
		bottom = {};
		front = {};
		top.x = bird.x;
		top.y = bird.y - 10;
		bottom.x = bird.x;
		bottom.y = bird.y + 10;
		front.x = bird.x + 10;
		front.y = bird.y;

		if(top.x >= this.x && top.x <= this.x + this.pipeWidth && top.y <= this.top){
			return true;
		}
		if(bottom.x >= this.x && bottom.x <= this.x + this.pipeWidth && bottom.y >= this.bottom){
			return true;
		}
		if(front.x >= this.x && front.x <= this.x + this.pipeWidth && (front.y >= this.bottom || front.y <= this.top)) {
			return true;
		}
		return false;
	}

	// this.score = function(){
	// 	if(!this.scored && this.x < 200){
	// 		score++;
	// 		this.scored = true;
	// 	}
	// }
}


function mutate(x) {
  if (random(1) < 0.1) {
    let offset = randomGaussian() * 0.5;
    let newx = x + offset;
    return newx;
  } else {
    return x;
  }
}
