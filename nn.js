function NeuralNetwork(inputs, neurons, outputs){
	this.inputs = inputs;
	this.outputs = outputs;
	this.neurons = neurons;
	this.h_biases=[];
	this.h_weights=[];
	this.o_biases=[];
	this.o_weights=[];

	for (var i = 0; i < neurons; i++) {
		this.h_biases.push(random() * 2 - 1);
	}

	for (var i = 0; i < neurons; i++) {
		this.h_weights.push([]);
		for (var j = 0; j < inputs; j++) {
			this.h_weights[i].push(random() * 2 - 1)
		}
	}

	for (var i = 0; i < outputs; i++) {
		this.o_biases.push(random() * 2 - 1);
	}

	for (var i = 0; i < outputs; i++) {
		this.o_weights.push([]);
		for (var j = 0; j < neurons; j++) {
			this.o_weights[i].push(random() * 2 - 1)
		}
	}

	this.sigmoid = function(x){
		return 1/(1 + exp(-x));
	}

	this.predict = function(input){
		var h_results = [];
		var o_results = [];
		
		for (var i = 0; i < this.neurons; i++) {
			var a = 0;
			for (var j = 0; j < this.inputs; j++) {
				a += input[j] * this.h_weights[i][j];
			}
			a += this.h_biases[i];
			h_results.push(this.sigmoid(a));
		}

		for (var i = 0; i < this.outputs; i++) {
			var a = 0;
			for (var j = 0; j < this.neurons; j++) {
				a += h_results[j] * this.o_weights[i][j];
			}
			a += this.o_biases[i];
			o_results.push(this.sigmoid(a));
		}

		return o_results;
	}


	this.mutate = function(func){
		this.h_biases = this.h_biases.map(func)
		this.o_biases = this.h_biases.map(func)
		this.h_weights = this.h_weights.map(function(row){
			return row.map(func)
		})
		this.o_weights = this.o_weights.map(function(row){
			return row.map(func)
		})
	}

	this.copy = function(){
		var copy = new NeuralNetwork(this.inputs, this.neurons, this.outputs);
		copy.h_weights = this.h_weights.map(function(arr) {
		    return arr.slice();
		});
		copy.h_biases = this.h_biases.slice();
		copy.o_weights = this.o_weights.map(function(arr) {
		    return arr.slice();
		});
		copy.o_biases = this.o_biases.slice();
		return copy;
	}

	this.combine = function(another){
		var child = this.copy();
		
		for (var i = 0; i < this.neurons; i++) {
			for (var j = 0; j < this.inputs; j++) {
				if((i+j) % 2 == 0){
					child.h_weights[i][j] = another.h_weights[i][j];
				}
			}
		}

		for (var i = 0; i < this.outputs; i++) {
			for (var j = 0; j < this.neurons; j++) {
				if((i+j) % 2 == 0){
					child.o_weights[i][j] = another.o_weights[i][j];
				}
			}
		}

		for (var i = 0; i < this.neurons; i += 2) {
			child.h_biases[i] = another.h_biases[i];
		}

		for (var i = 0; i < this.outputs; i += 2) {
			child.o_biases[i] = another.o_biases[i];
		}
		return child;
	}
}
